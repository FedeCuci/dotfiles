"Make space leader character
nnoremap <SPACE> <Nop>
let mapleader = "\<Space>"

" Do not delay when inserting in normal mode
set ttimeoutlen=0
set colorcolumn=80

" Use system clipboard
"set clipboard+=unnamedplus

" Enable tab completion
set wildmode=longest,list,full
set nocompatible
set nobackup
set noswapfile

set termguicolors

" VimWiki dependencies
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
filetype plugin on
let g:instant_markdown_autostart = 0

filetype indent plugin on
 
" Enable syntax highlighting
syntax on

" Start NODEtree when vim opens

map <C-m> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

set hidden

" Better command-line completion
set wildmenu
 
" Dont end lines
"set nowrap

set textwidth=0 wrapmargin=0 " https://stackoverflow.com/questions/2280030/how-to-stop-line-breaking-in-vim

" Highlight searches (use <C-L> to temporarily turn off highlighting; see the
" mapping of <C-L> below)
" set hlsearch
set incsearch

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase
 
set autoindent
 
set ruler
 
set confirm
 
" Use visual bell instead of beeping when doing something wrong
set visualbell

" Enable use of the mouse for all modes
set mouse=a

" Display column and row of cursor
set cursorline
set cursorcolumn

set number relativenumber

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
set notimeout ttimeout ttimeoutlen=200

" Tab options
set shiftwidth=4
set softtabstop=4
set expandtab
set tabstop=4
set smartindent  

" Put new vertical split to the right and new horizontal split at the botton
set splitbelow splitright

" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$

nnoremap <Leader>o o<Esc>
nnoremap <Leader>O O<Esc> " Spacebar and O for new line above without entering insert mode
nnoremap <Leader>q :q<CR>
nnoremap <Leader>Q :q!<CR>
nnoremap <Leader>s :w<ESC>h " Mapped to s instead of w to prevent clash with vimwiki bindings

" Create vim splits
nnoremap <Leader>\ :vs<CR>
nnoremap <Leader>- :sp<CR>

" Navigate through vim splits using spacebar and vim keybindings
nnoremap <C-h> :wincmd h<CR>
nnoremap <C-l> :wincmd l<CR>
nnoremap <C-j> :wincmd j<CR>
nnoremap <C-k> :wincmd k<CR>
nnoremap <Leader>r ggdG:VimBeGood<CR> 
" nnoremap <Enter> @="m`O\eg``"<cr>

" Might be worth deleting
nnoremap <Leader><Leader> i<space><Right><ESC>
nnoremap <Leader>u :noh<CR> " https://vi.stackexchange.com/questions/184/how-can-i-clear-word-highlighting-in-the-current-document-e-g-such-as-after-se
nnoremap <Leader>; A;<ESC>

" Handling brackets: https://vim.fandom.com/wiki/Making_Parenthesis_And_Brackets_Handling_Easier
"inoremap { {}<ESC>i
"inoremap ( ()<ESC>i
"inoremap [ []<ESC>i
"inoremap ' ''<ESC>i
"inoremap < <><ESC>i

" Unbind arrow keys
noremap <Up>    <Nop>
noremap <Down>  <Nop>
noremap <Left>  <Nop>
noremap <Right> <Nop>

let g:ycm_key_list_stop_completion = ['<C-y>', '<CR>'] " https://stackoverflow.com/questions/19836637/how-to-accept-a-suggestion-from-vim-youcompleteme-plugin

let g:rustfmt_autosave = 1 " Enable automatic running of :RustFmt when saving a buffer

call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'vimwiki/vimwiki' " VimWiki for taking notes
Plug 'suan/vim-instant-markdown', {'for': 'markdown'} " View vimwiki markdown in browser
Plug 'preservim/nerdcommenter'
Plug 'gruvbox-community/gruvbox'
Plug 'rust-lang/rust.vim' " Plugin for rust programming
Plug 'ThePrimeagen/vim-be-good' " Practice VIM skills and speed
Plug 'mileszs/ack.vim'
Plug 'wellle/targets.vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

" :CocInstall coc-rust-analyzer to install rust-analyzer

colorscheme gruvbox " Call plugin functions after Plug#end()
set background=dark

" Improve coc vim functionality
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
